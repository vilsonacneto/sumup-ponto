defmodule Ponto.TesteView do
  use Ponto.Web, :view

  def render("show.v1.json", %{teste: teste}) do
    %{
      message: teste.message
    }
  end
end
