defmodule Ponto.Router do
  use Ponto.Web, :router
  alias Ponto.APIVersion

  pipeline :v1 do
    plug :accepts, ["json"]
    plug APIVersion, version: :v1
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  scope "/v1", Ponto do
    pipe_through :v1

    resources "/teste", TesteController
  end
end
