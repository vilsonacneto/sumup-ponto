defmodule Ponto.PageController do
  use Ponto.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
