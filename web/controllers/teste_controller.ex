defmodule Ponto.TesteController do
  use Ponto.Web, :controller

  def show(%{assigns: %{version: :v1}}=conn, _params) do
    teste = %{message: "Test"}
    render(conn, "show.v1.json", teste: teste)
  end

  def index(%{assigns: %{version: :v1}}=conn, _params) do
    render(conn, "show.v1.json", teste: %{message: "Testando"})
  end
end
