FROM elixir:onbuild

MAINTAINER Mineirinho <vilson.neto@sumup.com>

RUN mix local.hex --force

RUN mix archive.install --force https://github.com/phoenixframework/archives/raw/master/phoenix_new.ez

RUN mix deps.get

WORKDIR /ponto